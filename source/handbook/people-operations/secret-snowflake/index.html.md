---
layout: markdown_page
title: "Secret Snowflake"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Secret Snowflake 2018

We will be organizing a Secret Snowflake in 2018!

How does this work?:
- To participate join #secret-snowflake in slack by November 26, 2018. Please give a thumbs up emoji :)
- You'll receive an email to enroll in Secret Snowflake. Click the red link within the email to be taken to the platform where you can list your wishes. Enter your address (please add this as a wishlist item with the format Address: " "), and then find out who you will ship a present to. There are ads surrounding the wishlist section of the page. Please make sure to avoid those ads as you do not need to click any further to login. It is also helpful to add a website or link to your wishlist that allows for easy/cost effective international shipping. If you have any issues shipping the present to your buddy, feel free to reach out to People Ops in Slack to assist in finding a local retailer.  
- Get a present, and ship it!
  * The budget is € 20 for the gift (excluding shipping), paid out of pocket.
- Wait for your present to arrive.
- Finally! It arrived! Unpack it, take a picture, and thank your match in the #secret-snowflake channel on Slack to let them know that the gift has been received!
- Perfectly content, you wait until next year to participate again... :)
- If you have not received your gift by January 15th, or are unsure if your match received the gift you sent, please reach out to People Ops.

Note: By participating in Secret Snowflake you grant your match access to your address via People Ops.

### Organizing Secret Snowflake

1. Create a #secret-snowflake-(year) channel in Slack.
1. Announce the deadline on the company call and in the #general channel in Slack with a link to this page as well as a disclaimer that this activity gives People Ops the right to share your address with your match.
1. After the entry deadline, use an organizer platform to automatically create matches and send emails.
1. Test the platform: https://www.secretsantaorganizer.com/ before sending out all emails to participants.
1. Once the platform has been tested, add all emails, create matches, and start the campaign!
1. While Secret Snowflake is in process, follow-up with any issues that may arise.
1. After the January 15th deadline, ensure all participants received their present.

##### F.A.Q.

> But I have this super duper awesome gift idea and its over the budget

Well, this really depends on the situation and the fact that you actually have this question means you're probably not over budget by €2. ;)

> I found this amazing, best gift ever and its under budget?

That is perfectly fine. It is more important to give something awesome than to spend € 20.

> Wait, potentially I have to ship something to the other side of the world!

You don't have to, there is an Amazon, or something like it in most countries. Let them do the heavy lifting! If you can't come up with a solution, please ask @zj on Slack.

> Shipping my trebuchet will be expensive, can I take it with me to the next summit and give it there?

For a full size trebuchet I'll make an exception, other than that, please do not bring your gift to the next summit. This means a couple of things:
1. The other participant has to have some spare room in their suitcase
1. Some people will already have a gift, I wouldn't be able to handle the suspense!
1. No clue, just ship it!

> Can I add a poem to the package?

So, if you're referring to the good old Sinterklaasgedichten, yes! You can and should! Just keep in mind not everyone is familiar with tone usually used in them.

> Cool idea, but I'd rather not be a participant this year.

That is ok! No hard feelings, and maybe next year!
